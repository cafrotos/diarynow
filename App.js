import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'

import { VIEWS_NAME } from 'consts'
import {
  Main,
  DiaryDetail,
  GoalsScreen,
  CreateGoals,
  SelectActivities,
  RemindersScreen,
  ChoosenActivities,
  ChoosenMoods,
  WriteDiary,
  SubDashBoard,
  Search
} from 'views'

const AppNavigator = createStackNavigator(
  {
    [VIEWS_NAME.MAIN]: {
      screen: Main
    },
    [VIEWS_NAME.DIARY_DETAIL]: {
      screen: DiaryDetail
    },
    // Settings tab
    // Goals screen
    [VIEWS_NAME.GOALS]: {
      screen: GoalsScreen
    },
    [VIEWS_NAME.CREATE_GOALS]: {
      screen: CreateGoals
    },
    [VIEWS_NAME.SELECT_ACTIVITIES]: {
      screen: SelectActivities
    },
    // Reminder screen
    [VIEWS_NAME.REMINDERS]: {
      screen: RemindersScreen
    },
    // End settings tab
    // Create diary screen
    [VIEWS_NAME.CHOOSEN_MOODS]: {
      screen: ChoosenMoods
    },
    [VIEWS_NAME.CHOOSEN_ACTIVITIES]: {
      screen: ChoosenActivities
    },
    [VIEWS_NAME.WRITE_DIARY]: {
      screen: WriteDiary
    },
    // End create diary screen
    // SubDashboard
    [VIEWS_NAME.SUB_DASHBOARD]: {
      screen: SubDashBoard
    },
    // End sub dashboard
    // Search
    [VIEWS_NAME.SEARCH]: {
      screen: Search
    } 
  },
  {
    headerMode: "none",
  }
);

export default createAppContainer(AppNavigator);