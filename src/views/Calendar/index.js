import React, { Fragment } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Headers from 'components/Headers';
import MonthPicker from 'components/MonthPicker';
import { Calendar } from 'react-native-calendars';
import Realm, { createDiary, getDiaries } from 'repositories';
import moment from 'moment';
import { VIEWS_NAME } from 'consts';

import SubDashBoard from './SubDashBoard';

export { SubDashBoard }

const vacation = { key: 'vacation', color: 'red' };
const massage = { key: 'massage', color: 'blue' };
const workout = { key: 'workout', color: 'green' };

const mood = {
  happy: vacation,
  confuse: massage,
  surprise: workout,
  sad: vacation,
  worry: vacation,
  angry: vacation
}
export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: {},
      datetime: moment(),
      datetimeUpdate: moment()
    }
    Realm.addListener("change", async () => {
      const diaries = await getDiaries();
      this.setState({
        count: this.count(diaries)
      })
    })
  }

  async componentDidMount() {
    const diaries = await getDiaries();
    this.setState({
      count: this.count(diaries)
    })
  }

  componentDidUpdate() {
    const datetime = this.props.navigation.getParam("datetime");
    if (datetime && datetime.toString() !== this.state.datetimeUpdate.toString()) {
      this.setState({
        datetimeUpdate: moment(datetime),
        datetime: moment(datetime)
      });
    }
  }

  onDayPress = (day) => {
    this.props.navigation.navigate(VIEWS_NAME.SUB_DASHBOARD, { date: moment(day.timestamp) })
  }

  count = (diaries) => {
    let count = {};
    diaries.map((diary) => {
      let tol = moment(diary['diaryDate']).format("YYYY-MM-DD");

      if (count.hasOwnProperty(tol)) {
        count[tol]['dots'].add(mood[diary['mood'].toLowerCase()]);
      }
      else {
        count[tol] = { dots: new Set() };
        count[tol]['dots'].add(mood[diary['mood'].toLowerCase()]);
      }

    });
    for (var proper in count) {
      count[proper]['dots'] = Array.from(count[proper]['dots'])
    }
    return count;
  }

  setMonth = datetime => {
    this.setState({
      datetime
    })
    if (this.props.navigation.getParam("setDateTime")) {
      this.props.navigation.getParam("setDateTime")(datetime)
    }
  }

  onPressSearch = () => {
    this.props.navigation.navigate(VIEWS_NAME.SEARCH)
  }

  render() {

    return (
      <Fragment >
        <Headers right={true} onRightPress={this.onPressSearch}>
          <MonthPicker onSetDate={this.setMonth} date={this.state.datetime} />
        </Headers>
        <Calendar
          monthFormat={''}
          nextMonth={""}
          current={this.state.datetime.toDate()}
          theme={{
            backgroundColor: '#ffffff',
            calendarBackground: '#ffffff',
            selectedDayBackgroundColor: 'red',
            selectedDayTextColor: 'red',
            todayTextColor: '#00adf5',
            dayTextColor: 'rgb(46, 49, 49)',
            textDisabledColor: '#d9e1e8',
            indicatorColor: 'blue',
            dotColor: '#00adf5',
            selectedDotColor: 'red',
            arrowColor: 'orange',
            monthTextColor: 'red',
            textDayFontFamily: 'Courier',
            textMonthFontFamily: 'Courier',
            textDayHeaderFontFamily: 'Courier',
            textDayHeaderFontWeight: 'bold',
            textDayFontSize: 20,
            textMonthFontSize: 25,
            textDayHeaderFontSize: 16,
            height: 200,
            selectedDayBackgroundColor: 'blue',
            'stylesheet.calendar.header': {
              header: {
                height: 0
              }
            }
          }}
          hideExtraDays={true}
          renderArrow={() => null}
          markedDates={
            this.state.count
          }
          onDayPress={this.onDayPress}
          markingType={'multi-dot'}
        />
      </Fragment>
    );
  }
}
