import React from 'react';
import { View, Image } from 'react-native'

import DashboardBg from 'assets/images/bg_dashboard.png'
import Title from 'components/Title';
import { ORIGINAL } from 'consts/colors';
import { getIcon } from 'utils/Utils';
import { ICON_TYPE } from 'consts/icons';

const BlankDashboard = (props) => {
  return (
    <View style={{ display: "flex", alignItems: "center", justifyContent: "flex-end" }}>
      <Image source={DashboardBg} style={{ height: "45%", width: "40%" }} />
      <Title size="large" color={ORIGINAL.BLACK_WHITE.LEVEL2} weight="bold">
        No Diaries
      </Title>
      <Title color={ORIGINAL.BLACK_WHITE.LEVEL2} >
        This day has no diaries
      </Title>
    </View>
  )
}

export default BlankDashboard