import React, { Fragment } from 'react';
import { ScrollView, View } from 'react-native'
import { List } from 'react-native-paper'

import { getDiaries, createDiary } from 'repositories'
import CustomItem from 'components/CustomItem';
import Headers from 'components/Headers';
import moment from 'moment';
import MonthPicker from 'components/MonthPicker';
import { ORIGINAL } from 'consts/colors';
import { VIEWS_NAME } from 'consts';
import BlankDashboard from './BlankDashboard';

export default class SubDashboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      diaries: [],
      date: moment()
    }
  }

  componentDidMount() {
    const date = this.props.navigation.getParam("date")
    this.onFilterMonthYear(date)
  }

  /**
   * @param {import('moment').Moment} date
   */
  onFilterMonthYear = (date) => {
    getDiaries()
      .then(diaries => {
        const filterDiaries = diaries
          .filtered("diaryDate >= $0 AND diaryDate <= $1", moment(date).hour(0).minute(0).second(0).toDate(), moment(date).hour(23).minute(59).second(59).toDate())
        this.setState({
          diaries: filterDiaries,
          date
        })
      })
      .catch(err => console.log(err))
  }

  showDiaries = () => {
    if (this.state.diaries.length > 0) {
      const thisDiaries = {}
      this.state.diaries
        .map(diary => {
          if (thisDiaries[moment(diary.diaryDate).format("dddd, MMM DD")] instanceof Array) {
            thisDiaries[moment(diary.diaryDate).format("dddd, MMM DD")].push(diary)
          }
          else {
            thisDiaries[moment(diary.diaryDate).format("dddd, MMM DD")] = [];
            thisDiaries[moment(diary.diaryDate).format("dddd, MMM DD")].push(diary)
          }
        })
      return thisDiaries
    }
    return {}
  }

  onLeftPress = () => {
    this.props.navigation.goBack()
  }

  onPressDiary = (diaryId) => () => {
    this.props.navigation.navigate(VIEWS_NAME.DIARY_DETAIL, { diaryId })
  }

  render() {
    const diaries = this.showDiaries()
    return (
      <Fragment>
        <Headers left={true} onLeftPress={this.onLeftPress}>
          {this.state.date.format("dddd MM/DD/YYYY")}
        </Headers>
        {
          Object.keys(diaries).length > 0 ?
            <ScrollView>
              {
                Object.keys(diaries).map((day, index) => {
                  if (diaries[day] instanceof Array) {
                    return (
                      <List.Section
                        key={index}
                        title={day}
                        titleStyle={{ paddingTop: 10, paddingBottom: 0, fontWeight: "bold", color: "#000" }}
                      >
                        {
                          diaries[day].map((diary, index) => {
                            return (
                              <CustomItem
                                key={diary.id}
                                time={moment(diary.createdAt)}
                                timeline={true}
                                iconLeft={diary.mood}
                                midIcons={diary.activities.split(", ")}
                                content={diary.content}
                                onPress={this.onPressDiary(diary.id)}
                              />
                            )
                          })
                        }
                      </List.Section>
                    )
                  }
                  return null
                })
              }
              <View style={{ height: 30 }}></View>
            </ScrollView> :
            <BlankDashboard />
        }
      </Fragment>
    )
  }
} 