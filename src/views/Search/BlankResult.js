import React from 'react';
import { View, Image } from 'react-native'

import DashboardBg from 'assets/images/bg_search.png'
import Title from 'components/Title';
import { ORIGINAL } from 'consts/colors';
import { getIcon } from 'utils/Utils';
import { ICON_TYPE } from 'consts/icons';

const BlankDashboard = (props) => {
  return (
    <View style={{ display: "flex", alignItems: "center",width: "100%", position: "absolute", top: "40%" }}>
      <Image source={DashboardBg} style={{width: 200, height: 200}} />
    </View>
  )
}

export default BlankDashboard