import React, { Fragment } from 'react';
import { Searchbar, List, TextInput } from 'react-native-paper';
import { View, ScrollView, Keyboard, DatePickerAndroid } from 'react-native'

import Headers from 'components/Headers'
import { getDiaries } from 'repositories';
import BlankResult from './BlankResult';
import CustomItem from 'components/CustomItem';
import moment from 'moment';
import { VIEWS_NAME } from 'consts';
import { getIcon } from 'utils/Utils';
import CustomIconButon from 'components/CustomIconButton';

export default class Search extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      diaries: [],
    }
  }

  componentDidMount() {
    this.getDiaries()
  }

  getDiaries = (string) => {
    return getDiaries()
      .then(diaries => diaries.filtered("mood CONTAINS[c] $0 OR content CONTAINS[c] $1", string, string))
      .then(diaries => {
        if (this.state.fromDate) {
          return diaries.filtered("diaryDate >= $0", this.state.fromDate.toDate())
        }
        return diaries
      })
      .then(diaries => {
        if (this.state.toDate) {
          return diaries.filtered("diaryDate <= $0", this.state.toDate.toDate())
        }
        return diaries
      })
      .then(diaries => this.setState({ diaries }))
  }

  onPressGoBack = () => {
    this.props.navigation.goBack()
  }

  onPressDiary = (diaryId) => () => {
    this.props.navigation.navigate(VIEWS_NAME.DIARY_DETAIL, { diaryId })
  }

  showDiaries = () => {
    if (this.state.diaries.length > 0) {
      const thisDiaries = {}
      this.state.diaries
        .map(diary => {
          if (thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")] instanceof Array) {
            thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")].push(diary)
          }
          else {
            thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")] = [];
            thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")].push(diary)
          }
        })
      return thisDiaries
    }
    return {}
  }

  onPressPickDate = (type) => {
    return async () => {
      Keyboard.dismiss()
      try {
        const setState = {}
        const { action, year, month, day } = await DatePickerAndroid.open({
          date: new Date(),
        });

        if (action !== DatePickerAndroid.dismissedAction) {
          if (type === "from") {
            setState[`${type}Date`] = moment().year(year).month(month).date(day).hour(0).minute(0).second(0)
          }
          else {
            setState[`${type}Date`] = moment().year(year).month(month).date(day).hour(23).minute(59).second(59)
          }
          this.setState(setState)
        }
      } catch ({ code, message }) {
        console.warn('Cannot open date picker', message);
      }
    }
  }

  clearDate = (type) => () => {
    this.setState({
      [`${type}Date`]: null
    })
  }

  render() {
    const diaries = this.showDiaries()
    return (
      <Fragment>
        <Headers left={true} onLeftPress={this.onPressGoBack}>Search</Headers>
        <View style={{ padding: 10 }}>
          <Searchbar
            placeholder={"Search note by content, moods"}
            onChangeText={(text) => this.getDiaries(text)}
          />
          <View style={{ display: "flex", flexDirection: "row", width: "100%", justifyContent: "space-around", paddingTop: 10 }}>
            <View style={{ width: "45%" }}>
              <TextInput
                style={{ width: "100%" }}
                placeholder="From"
                value={this.state.fromDate && this.state.fromDate.format("DD/MM/YYYY")}
                onFocus={this.onPressPickDate("from")}
                mode="outlined"
              />
              <View style={{ position: "absolute", display: "flex", flexDirection: "row", alignItems: "center", height: "110%", right: -3 }}>
                {
                  this.state.fromDate ?
                    <CustomIconButon
                      icon={getIcon({ type: "FontAwesome", name: "remove" })}
                      style={{ zIndex: 10 }}
                      pressColor="#0082ff"
                      size={20}
                      onPress={this.clearDate("from")}
                    /> :
                    null
                }
              </View>
            </View>
            <View style={{ width: "45%" }}>
              <TextInput
                style={{ width: "100%" }}
                placeholder="To"
                value={this.state.toDate && this.state.toDate.format("DD/MM/YYYY")}
                onFocus={this.onPressPickDate("to")}
                mode="outlined"
              />
              <View style={{ position: "absolute", display: "flex", flexDirection: "row", alignItems: "center", height: "110%", right: -3 }}>
                {
                  this.state.toDate ?
                    <CustomIconButon
                      icon={getIcon({ type: "FontAwesome", name: "remove" })}
                      style={{ zIndex: 10 }}
                      pressColor="#0082ff"
                      onPress={this.clearDate("to")}
                      size={20}
                    /> :
                    null
                }
              </View>
            </View>
          </View>
        </View>
        {
          Object.keys(diaries).length === 0 ?
            <BlankResult /> :
            <ScrollView>
              {
                Object.keys(diaries).map((day, index) => {
                  if (diaries[day] instanceof Array) {
                    return (
                      <List.Section
                        key={index}
                        title={day}
                        titleStyle={{ paddingTop: 10, paddingBottom: 0, fontWeight: "bold", color: "#000" }}
                      >
                        {
                          diaries[day].map((diary) => {
                            return (
                              <CustomItem
                                key={diary.id}
                                time={moment(diary.diaryDate)}
                                timeline={true}
                                iconLeft={diary.mood}
                                midIcons={diary.activities.split(", ")}
                                content={diary.content}
                                onPress={this.onPressDiary(diary.id)}
                              />
                            )
                          })
                        }
                      </List.Section>
                    )
                  }
                  return null
                })
              }
              <View style={{ height: 30 }}></View>
            </ScrollView>
        }
      </Fragment>
    )
  }
}