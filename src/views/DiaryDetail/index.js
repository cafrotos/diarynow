import React from 'react';
import { View, Text, StyleSheet, ScrollView, ImageBackground } from 'react-native';
import { ORIGINAL } from 'consts/colors';
import { IconButton, Paragraph, Menu, Button, Portal, Dialog } from 'react-native-paper';
import { getIcon, getMoods, getBackgroundImage, getIconByTitle } from 'utils/Utils';

import { getDiaryById, removeDiary } from 'repositories';
import moment from 'moment';
import { VIEWS_NAME } from 'consts';
import Sound from 'react-native-sound'
import Title from 'components/Title';

// const Sound = require('react-native-sound')

Sound.setCategory('Playback');

export default class WriteDiary extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      diary: {},
      isMute: false
    }
  }

  async componentDidMount() {
    const diaryId = this.props.navigation.getParam("diaryId")
    const diary = await getDiaryById(diaryId);
    const image = getBackgroundImage(diary.mood);
    var player = new Sound(`s_${diary.mood.toLowerCase()}_${Math.floor(Math.random() * 1000 % 5) + 1}.mp3`, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log(error)
        this.setState({ player: null })
      }
      player.play()
    });
    this.setState({
      diary,
      bgImage: image,
      visible: false,
      player
    })
  }

  onPressBack = () => {
    if (this.state.player) {
      this.state.player.stop()
    }
    this.props.navigation.goBack()
  }

  onDeleteDiary = () => {
    this.setState({ visible: false, visibleDialog: true })
  }

  onEditDiary = () => {
    // if (this.state.player) {
    //   this.state.player.stop()
    // }
    this.setState({ visible: false })
    this.props.navigation.navigate(VIEWS_NAME.CHOOSEN_MOODS, { action: "UPDATE", diary: this.state.diary })
  }

  controlPlayer = () => {
    if (this.state.isMute && this.state.player) {
      this.state.player.play()
    }
    if (!this.state.isMute && this.state.player) {
      this.state.player.pause()
    }
    this.setState({ isMute: !this.state.isMute })
  }

  onDialogCancel = () => {
    this.hideDialog()
  }

  onDialogDiscard = () => {
    this.hideDialog()
  }

  onDialogDelete = async () => {
    await removeDiary(this.state.diary.id);
    this.onPressBack()
  }

  hideDialog = () => {
    this.setState({
      visibleDialog: !this.state.visibleDialog
    })
  }

  componentWillUnmount() {
    if (this.state.player) {
      this.state.player.stop()
    }
  }

  render() {
    return (
      <ImageBackground source={this.state.bgImage} style={{ width: '100%', height: '100%' }}>
        <View style={{ backgroundColor: "rgba(0,0,0,0.45)", width: '100%', height: '100%' }}>
          <Portal>
            <Dialog
              visible={this.state.visibleDialog}
              onDismiss={this.hideDialog}>
              <Dialog.Actions style={CustomStyle.dialogAction}>
                <View style={CustomStyle.dialogTitle}>
                  <Title color={ORIGINAL.BLACK_WHITE.LEVEL2} style={CustomStyle.textTitle}>
                    Are you sure to want to delete this diary?
                  </Title>
                </View>
                <View style={CustomStyle.dialogButtonAction}>
                  <View style={CustomStyle.dialogButtonActionLeft}>
                  </View>
                  <View style={CustomStyle.dialogButtonActionRight}>
                    <Button onPress={this.onDialogCancel}>
                      Cancel
                    </Button>
                    <Button onPress={this.onDialogDelete}>
                      Delete
                    </Button>
                  </View>
                </View>
              </Dialog.Actions>
            </Dialog>
          </Portal>
          <IconButton
            icon={(props) => getIcon({ type: "MaterialIcons", name: "arrow-back", color: ORIGINAL.BLACK_WHITE.LEVEL6, size: 30 })}
            style={{ position: "absolute", top: 10, left: 10, color: "#fff" }}
            color={"#0082FF"}
            onPress={this.onPressBack}
          />
          <View
            style={{ position: "absolute", top: 10, right: 10, color: "#fff" }}
          >
            <Menu
              visible={this.state.visible}
              onDismiss={() => this.setState({ visible: false })}
              anchor={
                <IconButton
                  icon={(props) => getIcon({ type: "MaterialCommunityIcons", name: "dots-horizontal", color: ORIGINAL.BLACK_WHITE.LEVEL6, size: 30 })}
                  color={"#0082FF"}
                  onPress={() => { this.setState({ visible: true }) }}
                />
              }
            >
              <Menu.Item
                onPress={this.onDeleteDiary}
                title="Delete"
                icon={p => getIcon({
                  type: "MaterialCommunityIcons",
                  name: "trash-can",
                  size: 30, color:
                    ORIGINAL.PRIMARY.LEVEL1
                })}
              />
              <Menu.Item
                onPress={this.onEditDiary}
                title="Edit" icon={p =>
                  getIcon({
                    type: "MaterialCommunityIcons",
                    name: "square-edit-outline",
                    size: 30,
                    color: ORIGINAL.PRIMARY.LEVEL1
                  })}
              />
            </Menu>
          </View>
          <View style={{ width: "100%", display: "flex", alignItems: "center", paddingTop: 5 }}>
            {getMoods(this.state.diary.mood)}
            <View style={{ paddingTop: 5, display: "flex", flexDirection: "row", width: "30%", flexWrap: "wrap", justifyContent: "center" }}>
              {
                this.state.diary.activities && this.state.diary.activities !== "" ?
                  this.state.diary.activities.split(", ").map((activity, index) => (
                    <View key={index} style={{ paddingLeft: 3, paddingRight: 3 }}>
                      {getIconByTitle(activity, 20)}
                    </View>
                  )) :
                  null
              }
            </View>
          </View>
          <ScrollView style={{ marginTop: 15 }} >
            <Paragraph style={{ color: "#e0e0e0", fontStyle: "italic", fontSize: 16, lineHeight: 30, paddingLeft: 20, paddingRight: 20, textAlign: "center" }}>
              {this.state.diary.diaryDate && moment(this.state.diary.diaryDate).format("HH:mm dddd MMM/DD/YYYY")}
            </Paragraph>
            <Paragraph style={{ color: ORIGINAL.BLACK_WHITE.LEVEL6, fontSize: 20, lineHeight: 30, paddingBottom: 30, paddingLeft: 20, paddingRight: 20 }}>
              {"\t\t\t\t" + this.state.diary.content}
            </Paragraph>
          </ScrollView>
          <IconButton
            icon={(props) => getIcon({ type: "MaterialCommunityIcons", name: this.state.isMute ? "volume-off" : "volume-high", color: ORIGINAL.BLACK_WHITE.LEVEL6, size: 30 })}
            style={{ position: "absolute", bottom: 10, left: 10, color: "#fff" }}
            color={"#0082FF"}
            onPress={this.controlPlayer}
          />
        </View>
      </ImageBackground>
    )
  }
}

const CustomStyle = StyleSheet.create({
  dialogAction: { flexDirection: "column", justifyContent: "space-between", alignItems: "stretch" },
  dialogTitle: { width: "100%", paddingBottom: 15, paddingTop: 10 },
  textTitle: { textAlign: "left", paddingLeft: 15, fontSize: 23, fontWeight: "bold" },
  dialogButtonAction: { display: "flex", flexDirection: "row", width: "100%", justifyContent: "space-between", paddingTop: 5 },
  dialogButtonActionLeft: { flexGrow: 10, alignItems: "flex-start" },
  dialogButtonActionRight: { display: "flex", flexDirection: "row", flexGrow: 1, justifyContent: "space-between" },
})