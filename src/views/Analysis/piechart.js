import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  processColor,
} from 'react-native';

// import {StackNavigator, SafeAreaView} from 'react-navigation';
import {PieChart} from 'react-native-charts-wrapper';
import { is } from '@babel/types';

 class PieChartScreen extends React.Component {

  constructor() {
    super();

    this.state = {
      _data: {},
      isOk: true,
      legend: {
          enabled: false
        
      },
      data: {
        dataSets: [{
          values: [
            {value: 10, label: 'Happy'},
            {value: 20, label: 'Surpise'},
            {value: 30, label: 'Confuse'},
            {value: 10, label: 'Sad'},
            {value: 10, label: 'Worry'},
            {value: 20, label: 'Angry'}
        ],
          label: 'Pie dataset',
          config: {
            colors: [processColor('#C0FF8C'),
            processColor('#FFF78C'),
            processColor('#FFD08C'), 
            processColor('#8CEAFF'), 
            processColor('#FFD08C'),
            processColor('#FFF78C')],
            valueTextSize: 16,
            valueTextColor: processColor('blue'),
            sliceSpace: 1,
            // selectionShift: 13,
            // xValuePosition: "OUTSIDE_SLICE",
            // yValuePosition: "OUTSIDE_SLICE",
            valueFormatter: "#.#'%'",
            valueLineColor: processColor('white'),
            
            // valueLinePart1Length: 0.5
          }
        }],
      },
    //   highlights: [{x:1}],
      description: {
        text: '',
        textSize: 15,
        textColor: processColor('darkgray'),
        
      }
    
    };
  };
  componentDidMount=()=>{
      let obj = JSON.parse(JSON.stringify(this.state.data))
   
      let data=this.props.data;
      let isOk=true;
      for(let proper in data){
        if(data[proper]!=0) isOk=false;
      }
      obj['dataSets'][0]['values']=[data['happy']==0?undefined:{value: data['happy'],label: "Happy"}, 
      data['surprise']==0?undefined:{value: data['surprise'],label:"Surprise"},
      data['confuse']==0?undefined:{value: data['confuse'],label:"Confuse"}, 
      data['sad']==0?undefined:{value: data['sad'], label:"Sad"}, 
      data['worry']==0? undefined:{value: data['worry'],label:"Worry"},
      data['angry']==0? undefined: {value: data['angry'],label:"Angry"}];

      this.setState({
          data: obj,
          _data: data,
          isOk: isOk
      })
      
  }
  componentDidUpdate(prevProps, prevState) {
  
    if(JSON.stringify(this.props.data) != JSON.stringify(this.state._data)){
      let obj = JSON.parse(JSON.stringify(this.state.data));
      let data= this.props.data
      let isOk=true;
      for(let proper in data){
        if(data[proper]!=0) isOk=false;
      }
      obj['dataSets'][0]['values']=[data['happy']==0?undefined:{value: data['happy'],label: "Happy"}, 
      data['surprise']==0?undefined:{value: data['surprise'],label:"Surprise"},
      data['confuse']==0?undefined:{value: data['confuse'],label:"Confuse"}, 
      data['sad']==0?undefined:{value: data['sad'], label:"Sad"}, 
      data['worry']==0? undefined:{value: data['worry'],label:"Worry"},
      data['angry']==0? undefined: {value: data['angry'],label:"Angry"}];
      // obj['dataSets'][0]['values']=[{y: data['happy']}, {y: data['surprise']}, {y: data['confuse']}, {y: data['sad']}, {y: data['worry']}, {y: data['angry']}];
      this.setState({
          data: obj,
          _data: this.props.data,
          isOk: isOk
      })

    }}
  render() {
    if(this.state.isOk==true){
      return <View></View>
    }
    return (
        <View style={styles.container_1}>
          <PieChart
            style={styles.chart_1}
            logEnabled={true}
            chartBackgroundColor={processColor('white')}
            data={this.state.data}
            legend={this.state.legend}
            description={this.state.description}
            // highlights={this.state.highlights}
            entryLabelColor={processColor('green')}
            entryLabelTextSize={13}
            drawEntryLabels={true}

            rotationEnabled={true}
            rotationAngle={45}
            usePercentValues={true}
            chartDescription={this.state.description}
            styledCenterText={{text:' ', color: processColor('blue'), size: 20}}
            // centerTextRadiusPercent={100}
            // holeRadius={40}
            holeColor={processColor('#f0f0f0')}
            transparentCircleRadius={45}
            transparentCircleColor={processColor('#f0f0f088')}
            // maxAngle={350}
            
          />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container_1: {
    // flex: 1,
    
  },
  chart_1: {
    // flex: 1
    height: 300
  }
});

export {PieChartScreen};