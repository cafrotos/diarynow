import React, { Fragment } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
} from 'react-native';
import { PieChartScreen } from './piechart';
import { ORIGINAL } from 'consts/colors';
import { MyBarChart } from './barchar';
import moment from 'moment';
import Realm, { getDiaries } from 'repositories';

import Headers from 'components/Headers';
import MonthPicker from 'components/MonthPicker';
import { VIEWS_NAME } from 'consts';
import BlankDashboard from './BlankDashboard';

export default class Dashboard extends React.Component {
  constructor() {
    super();

    this.state = {
      datetime: moment(),
      month: moment(),
      year: 2019,
      isLoad: false,
      diaries: "",
      listContent: [],
      count: {
        happy: 0,
        angry: 0,
        sad: 0,
        surprise: 0,
        worry: 0,
        confuse: 0,
      },
    };
    Realm.addListener("change", async () => {
      const diaries = await getDiaries();

      const count = this.countMood(diaries);

      this.setState({
        count: count,
        isLoad: true,
        diaries: diaries
      })
    })
  }

  componentDidUpdate() {
    const datetime = this.props.navigation.getParam("datetime");
    if (datetime && datetime.toString() !== this.state.datetime.toString()) {
      this.setState({
        month: moment(datetime),
        datetime: moment(datetime)
      }, () => {
        let count = this.countMood(this.state.diaries);
        this.setState({
          count: count
        })
      });
    }
  }

  setMonth = date => {
    this.setState({
      month: date,
    }, () => {
      let count = this.countMood(this.state.diaries);
      this.setState({
        count: count
      })
      if (this.props.navigation.getParam("setDateTime")) {
        this.props.navigation.getParam("setDateTime")(date)
      }
    });
  };

  async componentDidMount() {
    const diaries = await getDiaries();

    const count = this.countMood(diaries);

    this.setState({
      count: count,
      isLoad: true,
      diaries: diaries
    })
  }
  async UNSAFE_componentWillMount() {
    const diaries = await getDiaries();
    if (JSON.stringify(this.state.diaries) != JSON.stringify(diaries)) {
      const count = this.countMood(diaries)
      this.setState({
        count: count,
        isLoad: true,
        diaries: diaries
      })
    }
  }
  countMood = diaries => {
    let month = this.state.month.format("MM");
    let year = this.state.month.format("YYYY");
    let count = {
      happy: 0,
      angry: 0,
      sad: 0,
      surprise: 0,
      worry: 0,
      confuse: 0,
    };

    diaries.map(diary => {
      if (diary['diaryDate'].getFullYear() == year && (diary['diaryDate'].getMonth() + 1 == month)) {
        count[diary.mood.toLowerCase()] += 1;
      }
    })
    return count;
  };

  onPressSearch = () => {
    this.props.navigation.navigate(VIEWS_NAME.SEARCH)
  }

  render() {
    return (
      <Fragment>
        <Headers right={true} onRightPress={this.onPressSearch}>
          <MonthPicker onSetDate={this.setMonth} date={this.state.month} />
        </Headers>
        <ScrollView style={{ flex: 1, flexDirection: 'column', height: "100%" }}>
          <View style={styles.headerContainer}>
            <Text style={styles.header}>Moods Chart</Text>
          </View>

          {this.state.isLoad == true && <MyBarChart style={styles.barChart} data={this.state.count} />}
          <View style={styles.iconContainer}>
            <View style={{ marginLeft: 8 }}>
              <Image
                style={styles.icon}
                source={require('../../assets/images/emoji/happy.png')}
              />
              <Text style={styles.iconDes}>Happy</Text>
            </View>

            <View>
              <Image
                style={styles.icon}
                source={require('../../assets/images/emoji/surprise.png')}
              />
              <Text style={styles.iconDes}>Surprise</Text>
            </View>
            <View>
              <Image
                style={styles.icon}
                source={require('../../assets/images/emoji/confuse.png')}
              />
              <Text style={styles.iconDes}>Confuse</Text>
            </View>
            <View>
              <Image
                style={styles.icon}
                source={require('../../assets/images/emoji/sad.png')}
              />
              <Text style={styles.iconDes}>Sad</Text>
            </View>
            <View>
              <Image
                style={styles.icon}
                source={require('../../assets/images/emoji/worry.png')}
              />
              <Text style={styles.iconDes}>Worry</Text>
            </View>
            <View style={{ marginRight: 9 }}>
              <Image
                style={styles.icon}
                source={require('../../assets/images/emoji/angry.png')}
              />
              <Text style={styles.iconDes}>Angry</Text>
            </View>
          </View>
          {
            this.state.count.angry == 0 &&
              this.state.count.confuse == 0 &&
              this.state.count.happy == 0 &&
              this.state.count.sad == 0 &&
              this.state.count.surprise == 0 &&
              this.state.count.worry == 0 ?
              <View style={{ paddingTop: 30 }}>
                <BlankDashboard />
              </View>
              : <View style={styles.headerContainer}><Text style={styles.header}>Moods Percent</Text></View>

          }
          {
            this.state.count.angry == 0 &&
              this.state.count.confuse == 0 &&
              this.state.count.happy == 0 &&
              this.state.count.sad == 0 &&
              this.state.count.surprise == 0 &&
              this.state.count.worry == 0 ?
              <Text></Text> : <View>{this.state.isLoad == true && <PieChartScreen data={this.state.count} />}</View>
          }
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 0,
  },
  barChart: {
    height: 200,
  },
  chart: {
    height: 200,
    width: 200,
  },
  headerContainer: {
    height: 70,
  },
  header: {
    textAlign: 'left',
    paddingLeft: 10,
    height: 50,
    lineHeight: 50,
    fontWeight: 'bold',
    fontSize: 25,
  },
  icon: {
    width: 50,
    height: 50,
    marginLeft: 0,
  },
  iconDes: {
    // backgroundColor: "white",
    height: 30,
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    color: ORIGINAL.BLACK_WHITE.LEVEL1,
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'row',
    // height: 5,
    justifyContent: 'space-around',
    // backgroundColor: 'red'
  },
  calendarIcon: {
    marginTop: 10,
  },
  monthSelector: {
    height: 40,
    fontWeight: 'bold',
    lineHeight: 30,

    textAlign: 'center',

    borderBottomWidth: 1,
    color: ORIGINAL.PRIMARY.LEVEL3,
  },
  monthTitle: {
    fontFamily: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
    color: ORIGINAL.PRIMARY.LEVEL3,
    lineHeight: 30,
  },
});
