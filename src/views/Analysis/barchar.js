import React from 'react';
import {BarChart} from "react-native-charts-wrapper";
import {View,
    processColor,
    StyleSheet
} from "react-native";
import {ORIGINAL} from 'consts/colors';
class MyBarChart extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      _data: {},
      count:{"happy":0,"angry":0,"confuse":0,"sad":0,"surprise":0,"worry":0 },
      data: {
        dataSets: [
          {
            values: [{y: 10}, {y: 20}, {y: 30}, {y: 10}, {y: 10}, {y: 10}],
            label: 'Zero line dataset',

            config: {
              barWidth: 0.2,
              colors: [
                processColor(ORIGINAL.PRIMARY.LEVEL1),
                processColor(ORIGINAL.PRIMARY.LEVEL1),
                processColor(ORIGINAL.PRIMARY.LEVEL1),
                processColor(ORIGINAL.PRIMARY.LEVEL1),
                processColor(ORIGINAL.PRIMARY.LEVEL1),
                processColor(ORIGINAL.PRIMARY.LEVEL1),
              ],
            },
          },
        ],
      },
      xAxis: {
        valueFormatter: ['', '', '', '', '', ''],
        textSize: 12,
        textColor: processColor('black'),
        granularityEnabled: true,
        position: 'BOTTOM',
      },
      yAxis: {
        left: {
          drawLabels: false,
          drawAxisLine: false,
          drawGridLines: false,
          textSize: 16,
        },
        right: {
          enabled: false,
        },
      },
    };
  }
  componentDidMount=()=>{
    let obj = JSON.parse(JSON.stringify(this.state.data))
   
      let data=this.props.data;
      obj['dataSets'][0]['values']=[{y: data['happy']}, {y: data['surprise']}, {y: data['confuse']}, {y: data['sad']}, {y: data['worry']}, {y: data['angry']}];
      this.setState({
          data: obj
      })
      
  }
  componentDidUpdate(prevProps, prevState) {
  //  console.log(prevProps+ "adf")

    if(JSON.stringify(this.props.data) != JSON.stringify(this.state._data)){
      let obj = JSON.parse(JSON.stringify(this.state.data));
      
      // let data=this.state.count;
      let data= this.props.data
      obj['dataSets'][0]['values']=[{y: data['happy']}, {y: data['surprise']}, {y: data['confuse']}, {y: data['sad']}, {y: data['worry']}, {y: data['angry']}];
      this.setState({
          data: obj,
          _data: this.props.data
      })

    }
  }
  render(){
      return(
        <View>
        <BarChart
        style={styles.barChart}
        data={this.state.data}
        xAxis={this.state.xAxis}
        yAxis={this.state.yAxis}
        chartDescription={{text: ''}}
        legend={{enabled: false}}
      />
      </View>
      )
  }
}

const styles=StyleSheet.create({
    barChart:{
        height: 200
    }
})
export {MyBarChart}
