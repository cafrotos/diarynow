import React from 'react';
import { View, Text, StyleSheet, TextInput, ImageBackground, ScrollView } from 'react-native';
import { ORIGINAL } from 'consts/colors';
import { IconButton } from 'react-native-paper';
import * as Progress from 'react-native-progress';
import { getIcon, getMoods, getBackgroundImage } from 'utils/Utils';

import CustomIconButton from 'components/CustomIconButton'
import { createDiary, updateDiary } from 'repositories';
import { VIEWS_NAME } from 'consts';
import moment from 'moment';
import Toast from 'react-native-root-toast';

export default class WriteDiary extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      content: "",
      mood: "",
      diaryDate: moment(),
      activities: [],
      textColor: "white",
      isLoading: false,
    }
  }

  componentDidMount() {
    const action = this.props.navigation.getParam("action")
    const diary = this.props.navigation.getParam("diary")
    if (action === "UPDATE") {
      const image = getBackgroundImage(diary.mood);
      return this.setState({
        action,
        diary,
        content: diary.content,
        mood: diary.mood,
        bgImage: image
      })
    }
    const mood = this.props.navigation.getParam("mood")
    const activities = this.props.navigation.getParam("activities")
    const diaryDate = this.props.navigation.getParam("diaryDate")
    const image = getBackgroundImage(mood);
    this.setState({ mood, activities, diaryDate, bgImage: image })
  }

  onChangeText = (content) => {
    this.setState({ content })
  }

  onPressBack = () => {
    this.props.navigation.goBack()
  }

  onSave = async () => {
    if (!this.state.content || this.state.content.trim() === "") {
      return Toast.show("Please input your diary!", {
        duration: Toast.durations.SHORT,
        position: Toast.positions.TOP,
        backgroundColor: "red"
      })
    }
    this.setState({ isLoading: true })
    if (this.state.action === "UPDATE") {
      const diary = this.state.diary;
      diary.content = this.state.content;
      diary.activities = diary.activities.join(", ")
      try {
        await updateDiary(diary);
        this.setState({ isLoading: false })
        this.props.navigation.navigate(VIEWS_NAME.DASHBOARD)
      } catch (error) {
        console.log(error)
      }
      return;
    }
    const diary = {
      content: this.state.content,
      activities: this.state.activities.join(", "),
      mood: this.state.mood,
      diaryDate: this.state.diaryDate.toDate()
    }
    try {
      await createDiary(diary);
      this.setState({ isLoading: false })
      this.props.navigation.navigate(VIEWS_NAME.MAIN)
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    return (
      <ImageBackground source={this.state.bgImage} style={{ width: '100%', height: '100%' }}>
        <View style={{ backgroundColor: "rgba(0, 0, 0, 0.4)", height: "100%", width: "100%" }}>
          {
            this.state.isLoading ?
              <View style={{ backgroundColor: "rgba(0, 0, 0, 0.7)", height: "100%", width: "100%", position: "absolute", zIndex: 2, display: "flex", justifyContent: "center", alignItems: "center" }}>
                <View style={{ height: 100, width: 100 }}>
                  <Progress.CircleSnail color={[ORIGINAL.PRIMARY.LEVEL1, ORIGINAL.COMPLEMENTARY.LEVEL1, ORIGINAL.SECOND_A.LEVEL1, ORIGINAL.SECOND_B.LEVEL1]} size={100} thickness={8} />
                </View>
              </View> :
              null
          }
          <IconButton
            icon={(props) => getIcon({ type: "MaterialIcons", name: "arrow-back", color: "#ffffff", size: 30 })}
            style={{ position: "absolute", top: 10, left: 10, color: "#ffffff" }}
            color={"#ffffff"}
            onPress={this.onPressBack}
          />
          <View style={{ position: "absolute", top: 5, left: "50%", transform: [{ translateX: -30 }] }}>
            {getMoods(this.state.mood)}
          </View>
          <ScrollView style={{ marginTop: 80 }}>
            <TextInput
              value={this.state.content}
              multiline={true}
              style={{ ...CustomStyle.textInput, color: this.state.textColor, lineHeight: 30, paddingLeft: 20, paddingRight: 20 }}
              placeholderTextColor={this.state.textColor}
              placeholder="Write here..."
              onChangeText={this.onChangeText}
            />
          </ScrollView>
          <View style={{ position: "absolute", bottom: 20, right: "50%", transform: [{ translateX: 35 }] }}>
            <CustomIconButton
              icon={getIcon({ type: "MaterialCommunityIcons", name: "check-all", size: 30, color: "white" })}
              color="#fff"
              pressColor="#0082ff"
              size={50}
              style={{ backgroundColor: "#0082FF", borderRadius: 25 }}
              onPress={this.onSave}
            />
          </View>
          <View style={{ position: "absolute", bottom: 10, right: "50%", transform: [{ translateX: 15 }] }}>
            <Text style={{ color: "#ffffff" }}>Save</Text>
          </View>
        </View>
      </ImageBackground>
    )
  }
}

const CustomStyle = StyleSheet.create({
  textInput: {
    backgroundColor: 'transparent',
    fontSize: 20,
    textAlign: "left"
  }
})