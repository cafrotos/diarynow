import React, { Fragment } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { IconButton, Button, Title } from 'react-native-paper'
import Style from './style'
import CustomIconButton from 'components/CustomIconButton';
import { getMoods, getIconByTitle, getIcon } from 'utils/Utils';
import { ORIGINAL } from 'consts/colors';
import { VIEWS_NAME } from 'consts';

export default class ChoosenActivities extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mood: "",
      activities: []
    }
  }

  componentDidMount() {
    const action = this.props.navigation.getParam("action")
    const diary = this.props.navigation.getParam("diary")
    if (action === "UPDATE") {
      return this.setState({
        action,
        diary,
        mood: diary.mood,
        activities: diary.activities.split(", ")
      })
    }
    const mood = this.props.navigation.getParam("mood");
    const diaryDate = this.props.navigation.getParam("diaryDate")
    if (mood) {
      this.setState({
        mood,
        diaryDate
      })
    }
  }

  onPressBack = () => {
    this.props.navigation.goBack()
  }

  onPickActivities = activity => () => {
    const activities = this.state.activities;
    if (activities.includes(activity)) {
      activities.splice(activities.indexOf(activity), 1);
    }
    else {
      activities.push(activity)
    }
    this.setState(activities)
  }

  onChoosenActivities = () => {
    if(this.state.action && this.state.action === "UPDATE") {
      const diary = this.state.diary;
      diary.activities = this.state.activities;
      return this.props.navigation.navigate(VIEWS_NAME.WRITE_DIARY, { action: this.state.action, diary: this.state.diary })
    }
    this.props.navigation.navigate(VIEWS_NAME.WRITE_DIARY, { mood: this.state.mood, activities: this.state.activities, diaryDate: this.state.diaryDate })
  }

  render() {
    return (
      <View style={{ height: "100%" }}>
        <IconButton
          icon={(props) => (
            <MaterialIcons size={35} name="arrow-back" color={ORIGINAL.PRIMARY.LEVEL1} />
          )}
          style={{ position: "absolute", top: 10, left: 10, color: "#0082FF" }}
          color={"#0082FF"}
          onPress={this.onPressBack}
        />
        <View style={{ position: "absolute", top: 5, left: "50%", transform: [{ translateX: -30 }] }}>
          {getMoods(this.state.mood)}
        </View>
        <View style={{ height: 300, paddingLeft: 20, paddingRight: 20, position: "absolute", width: "100%", bottom: "37%" }}>
          <View style={{ display: "flex", justifyContent: "center", alignItems: "center", height: 100, marginBottom: Math.round(Dimensions.get("window").height * 0.05) }}>
            <Text style={{ fontSize: 35, color: "#000" }}>
              What have you
          </Text>
            <Text style={{ fontSize: 35, color: "#000" }}>
              been up to?
          </Text>
          </View>
          <View style={{ display: "flex", flexDirection: "row", justifyContent: "space-around" }}>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Reading", 30, this.state.activities.includes("Reading") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Reading") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Reading") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Reading")}
              />
              <Text>Reading</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Gaming", 30, this.state.activities.includes("Gaming") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Gaming") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Gaming") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Gaming")}
              />
              <Text>Gaming</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Relax", 30, this.state.activities.includes("Relax") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Relax") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Relax") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Relax")}
              />
              <Text>Relax</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Friends", 30, this.state.activities.includes("Friends") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Friends") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Friends") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Friends")}
              />
              <Text>Friends</Text>
            </View>
          </View>
          <View style={{ display: "flex", flexDirection: "row", justifyContent: "space-around" }}>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Sport", 30, this.state.activities.includes("Sport") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Sport") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Sport") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Sport")}
              />
              <Text>Sports</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Party", 30, this.state.activities.includes("Party") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Party") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Party") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Party")}
              />
              <Text>Party</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Movies", 30, this.state.activities.includes("Movies") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Movies") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Movies") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Movies")}
              />
              <Text>Movies</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Shopping", 30, this.state.activities.includes("Shopping") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Shopping") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Shopping") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Shopping")}
              />
              <Text>Shopping</Text>
            </View>
          </View>
          <View style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Family", 30, this.state.activities.includes("Family") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Family") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Family") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Family")}
              />
              <Text>Family</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={getIconByTitle("Date", 30, this.state.activities.includes("Date") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1)}
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("Date") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("Date") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
                onPress={this.onPickActivities("Date")}
              />
              <Text>Date</Text>
            </View>
            <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <CustomIconButton
                icon={<MaterialIcons name="add" color="#0082FF" size={30} />}
                name="book-open-page-variant"
                size={55}
                color="#0082FF"
                style={{
                  borderColor: !this.state.activities.includes("-page") ? "#d0d0d0" : ORIGINAL.PRIMARY.LEVEL1,
                  backgroundColor: !this.state.activities.includes("-page") ? ORIGINAL.BLACK_WHITE.LEVEL6 : ORIGINAL.PRIMARY.LEVEL1,
                  borderWidth: 1,
                  borderRadius: 30
                }}
                pressColor="#0082FF"
              />
              <Text>Edit/Add</Text>
            </View>
          </View>
        </View>
        <View style={{ position: "absolute", bottom: 20, right: 20 }}>
          <CustomIconButton
            name="mode-edit"
            color="#0082ff"
            pressColor="#0082ff"
            size={50}
            style={{ backgroundColor: "white", borderRadius: 25, borderWidth: 1, borderColor: ORIGINAL.BLACK_WHITE.LEVEL5 }}
          />
        </View>
        <View style={{ position: "absolute", bottom: 10, right: 20 }}>
          <Text style={{ color: "#0082FF" }}>Edit Moods</Text>
        </View>
        <View style={{ position: "absolute", bottom: 20, right: "50%", transform: [{ translateX: 35 }] }}>
          <CustomIconButton
            icon={getIcon({ type: "MaterialIcons", name: "navigate-next", size: 30, color: "white" })}
            color="#fff"
            pressColor="#0082ff"
            size={50}
            style={{ backgroundColor: "#0082FF", borderRadius: 25 }}
            onPress={this.onChoosenActivities}
          />
        </View>
        <View style={{ position: "absolute", bottom: 10, right: "50%", transform: [{ translateX: 15 }] }}>
          <Text style={{ color: "#0082FF" }}>Next</Text>
        </View>
      </View>
    )
  }
}