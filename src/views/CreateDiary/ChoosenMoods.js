import React from 'react';
import { View, Text, TimePickerAndroid, DatePickerAndroid, Keyboard } from 'react-native'
import { IconButton } from 'react-native-paper'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import CustomIconButton from 'components/CustomIconButton'
import { VIEWS_NAME } from 'consts'
import Style from './style'
import { TextInput } from 'react-native-paper';
import moment from "moment"
import { ORIGINAL } from 'consts/colors';
import { getMoods, getIcon } from 'utils/Utils';

export default class ChoosenMoods extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      diaryDate: moment()
    };
  }

  componentDidMount() {
    const action = this.props.navigation.getParam("action")
    const diary = this.props.navigation.getParam("diary")
    if (action === "UPDATE") {
      this.setState({
        action,
        diary,
        diaryDate: moment(diary.diaryDate)
      })
    }
  }

  onDatePress = async (event) => {
    Keyboard.dismiss()
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: this.state.diaryDate.toDate(),
      });

      if (action !== DatePickerAndroid.dismissedAction) {
        const diaryDate = this.state.diaryDate.year(year).month(month).date(day)
        this.setState({ diaryDate })
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  }

  onTimePress = async () => {
    Keyboard.dismiss()
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        time: this.state.diaryDate.toDate(),
      });

      if (action !== DatePickerAndroid.dismissedAction) {
        const diaryDate = this.state.diaryDate.hour(hour).minute(minute)
        this.setState({ diaryDate })
      }
    } catch ({ code, message }) {
      console.warn('Cannot open time picker', message);
    }
  }

  onPressBack = () => {
    this.props.navigation.goBack()
  }

  onPressMood = mood => () => {
    if (this.state.action && this.state.action === "UPDATE") {
      const diary = JSON.parse(JSON.stringify(this.state.diary));
      diary.mood = mood;
      return this.props.navigation.navigate(VIEWS_NAME.CHOOSEN_ACTIVITIES, { action: this.state.action, diary })
    }
    this.props.navigation.navigate(VIEWS_NAME.CHOOSEN_ACTIVITIES, { mood, diaryDate: this.state.diaryDate })
  }

  render() {
    return (
      <View style={{ ...Style.column, height: "100%" }}>
        <IconButton
          icon={(props) => (
            <MaterialIcons size={35} name="arrow-back" color={ORIGINAL.PRIMARY.LEVEL1} />
          )}
          style={{ position: "absolute", top: 10, left: 10, color: "#0082FF" }}
          color={"#0082FF"}
          onPress={this.onPressBack}
        />
        <View style={Style.header}>
          <Text style={Style.title}>
            How are you?
          </Text>
        </View>
        <View style={{ display: "flex", flexDirection: "row", justifyContent: "space-around", alignItems: "center", width: "100%", paddingLeft: 20, paddingRight: 20 }}>
          <View style={{ dislay: "flex", flexDirection: "row" }}>
            <TextInput
              value={this.state.diaryDate.format("dddd, MM Do YYYY")}
              style={{ backgroundColor: "#fff", width: 220, height: 50, color: "#0082FF" }}
              onFocus={this.onDatePress}
              theme={{
                colors: {
                  text: "#0082ff"
                }
              }}
            />
            <MaterialIcons name="arrow-drop-down" style={{ fontSize: 40, marginTop: 10, position: "absolute", right: -5 }} />
          </View>
          <View style={{ marginLeft: 0, dislay: "flex", flexDirection: "row" }}>
            <TextInput
              value={this.state.diaryDate.format("kk:mm")}
              style={{ backgroundColor: "#fff", width: 80, height: 50 }}
              onFocus={this.onTimePress}
              theme={{
                colors: {
                  text: "#0082ff"
                }
              }}
            />
            <MaterialIcons name="arrow-drop-down" size={35} style={{ marginTop: 10, position: "absolute", right: -5 }} />
          </View>
        </View>
        <View style={Style.column, Style.footer}>
          <View style={Style.row}>
            <View>
              <IconButton
                icon={(props) => getMoods("angry")}
                style={{ height: 65, width: 65, paddingLeft: 35, paddingRight: 35 }}
                color={"#0082FF"}
                onPress={this.onPressMood("Angry")}
              />
              {
                this.state.action && this.state.action === "UPDATE" && this.state.diary.mood.toLowerCase() === "angry" ?
                  <View style={{ position: "absolute", bottom: 3, right: 0, }}>
                    {getIcon({ type: "MaterialCommunityIcons", name: "check-bold", size: 25, color: ORIGINAL.COMPLEMENTARY.LEVEL1 })}
                  </View> :
                  null
              }
            </View>
            <View>
              <IconButton
                icon={(props) => getMoods("confuse")}
                style={{ height: 65, width: 65, paddingLeft: 35, paddingRight: 35 }}
                color={"#0082FF"}
                onPress={this.onPressMood("Confuse")}
              />
              {
                this.state.action && this.state.action === "UPDATE" && this.state.diary.mood.toLowerCase() === "confuse" ?
                  <View style={{ position: "absolute", bottom: 3, right: 0, }}>
                    {getIcon({ type: "MaterialCommunityIcons", name: "check-bold", size: 25, color: ORIGINAL.COMPLEMENTARY.LEVEL1 })}
                  </View> :
                  null
              }
            </View>
            <View>
              <IconButton
                icon={(props) => getMoods("happy")}
                style={{ height: 65, width: 65, paddingLeft: 35, paddingRight: 35 }}
                color={"#0082FF"}
                onPress={this.onPressMood("Happy")}
              />
              {
                this.state.action && this.state.action === "UPDATE" && this.state.diary.mood.toLowerCase() === "happy" ?
                  <View style={{ position: "absolute", bottom: 3, right: 0, }}>
                    {getIcon({ type: "MaterialCommunityIcons", name: "check-bold", size: 25, color: ORIGINAL.COMPLEMENTARY.LEVEL1 })}
                  </View> :
                  null
              }
            </View>
          </View>
          <View style={Style.row}>
            <View>
              <IconButton
                icon={(props) => getMoods("sad")}
                style={{ height: 65, width: 65, paddingLeft: 35, paddingRight: 35 }}
                color={"#0082FF"}
                onPress={this.onPressMood("Sad")}
              />
              {
                this.state.action && this.state.action === "UPDATE" && this.state.diary.mood.toLowerCase() === "sad" ?
                  <View style={{ position: "absolute", bottom: 3, right: 0, }}>
                    {getIcon({ type: "MaterialCommunityIcons", name: "check-bold", size: 25, color: ORIGINAL.COMPLEMENTARY.LEVEL1 })}
                  </View> :
                  null
              }
            </View>
            <View>
              <IconButton
                icon={(props) => getMoods("surprise")}
                style={{ height: 65, width: 65, paddingLeft: 35, paddingRight: 35 }}
                color={"#0082FF"}
                onPress={this.onPressMood("Surprise")}
              />
              {
                this.state.action && this.state.action === "UPDATE" && this.state.diary.mood.toLowerCase() === "surprise" ?
                  <View style={{ position: "absolute", bottom: 3, right: 0, }}>
                    {getIcon({ type: "MaterialCommunityIcons", name: "check-bold", size: 25, color: ORIGINAL.COMPLEMENTARY.LEVEL1 })}
                  </View> :
                  null
              }
            </View>
            <View>
              <IconButton
                icon={(props) => getMoods("worry")}
                style={{ height: 65, width: 65, paddingLeft: 35, paddingRight: 35 }}
                color={"#0082FF"}
                onPress={this.onPressMood("Worry")}
              />
              {
                this.state.action && this.state.action === "UPDATE" && this.state.diary.mood.toLowerCase() === "worry" ?
                  <View style={{ position: "absolute", bottom: 3, right: 0, }}>
                    {getIcon({ type: "MaterialCommunityIcons", name: "check-bold", size: 25, color: ORIGINAL.COMPLEMENTARY.LEVEL1 })}
                  </View> :
                  null
              }
            </View>
          </View>
        </View>
        <View style={{ position: "absolute", bottom: 20, right: 20 }}>
          <CustomIconButton
            name="mode-edit"
            color="#fff"
            pressColor="red"
            size={50}
            onPress={() => { this.props.navigation.navigate(VIEWS_NAME.WRITE_DIARY) }}
            style={{ backgroundColor: "#0082FF", borderRadius: 25 }}
          />
        </View>
        <View style={{ position: "absolute", bottom: 10, right: 20 }}>
          <Text style={{ color: "#0082FF" }}>Edit Moods</Text>
        </View>
      </View>
    )
  }
}