import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  wrapperPage: {
    position: "relative"
  },
  title: {
    fontSize: 40,  
  },
  column: {
    display: "flex",
    flexDirection: "column" 
  },
  row: {
    display: "flex",
    flexDirection: "row"
  },
  header: {
    height: "30%",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  footer: {
    paddingTop: 50,
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  iconRowFirst: {
    marginBottom: 20
  },
  edit_moods: {
    height: 30,
    marginBottom: 20
  }
})