import React, { Fragment } from 'react';
import { ScrollView, View } from 'react-native'
import { List } from 'react-native-paper'
import firebase from 'react-native-firebase';
import type, { Notification } from 'react-native-firebase';

import Realm, { getDiaries, createDiary } from 'repositories'
import CustomItem from 'components/CustomItem';
import Headers from 'components/Headers';
import moment from 'moment';
import MonthPicker from 'components/MonthPicker';
import { ORIGINAL } from 'consts/colors';
import { VIEWS_NAME } from 'consts';
import BlankDashboard from './BlankDashboard';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      diaries: [],
      date: moment(),
      datetime: moment(),
    }
    Realm.addListener("change", () => {
      this.onFilterMonthYear(this.state.date)
    })
  }

  componentDidMount() {
    this.onFilterMonthYear(this.state.date)
    this.createNotificationChannel();
    this.checkPermission();
  }

  createNotificationChannel = () => {
    // Build a android notification channel
    const channel = new firebase.notifications.Android.Channel(
      "reminder", // channelId
      "Reminders Channel", // channel name
      firebase.notifications.Android.Importance.High // channel importance
    ).setDescription("Used for getting reminder notification"); // channel description
    // Create the android notification channel
    firebase.notifications().android.createChannel(channel);
  };

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      // We've the permission
      this.notificationListener = firebase
        .notifications()
        .onNotification(async notification => {
          // Display your notification
          await firebase.notifications().displayNotification(notification);
        });
    } else {
      // user doesn't have permission
      try {
        await firebase.messaging().requestPermission();
      } catch (error) {
        Alert.alert("Unable to access the Notification permission. Please enable the Notification Permission from the settings");
      }
    }
  };

  componentDidUpdate() {
    const datetime = this.props.navigation.getParam("datetime");
    if (datetime && datetime.toString() !== this.state.datetime.toString()) {
      getDiaries()
        .then(diaries => {
          const filterDiaries = diaries
            .filtered("diaryDate >= $0 AND diaryDate <= $1", moment(datetime).date(1).hour(0).minute(0).second(0).toDate(), moment(datetime).add(1, "month").date(0).hour(23).minute(59).second(59).toDate())
          this.setState({
            diaries: filterDiaries,
            date: datetime,
            datetime: datetime
          })
        })
        .catch(err => console.log(err))
    }
  }


  buildNotification = () => {
    const title = Platform.OS === "android" ? "Daily Reminder" : "";
    const notification = new firebase.notifications.Notification()
      .setNotificationId("1") // Any random ID
      .setTitle(title) // Title of the notification
      .setBody("This is a notification") // body of notification
      .android.setPriority(firebase.notifications.Android.Priority.High) // set priority in Android
      .android.setChannelId("reminder") // should be the same when creating channel for Android
      .android.setAutoCancel(true); // To remove notification when tapped on it
    return notification;
  };

  /**
   * @param {import('moment').Moment} date
   */
  onFilterMonthYear = (date) => {
    getDiaries()
      .then(diaries => {
        const filterDiaries = diaries
          .filtered("diaryDate >= $0 AND diaryDate <= $1", moment(date).date(1).hour(0).minute(0).second(0).toDate(), moment(date).add(1, "month").date(0).hour(23).minute(59).second(59).toDate())
        this.setState({
          diaries: filterDiaries,
          date
        })
      })
      .catch(err => console.log(err))
    if (this.props.navigation.getParam("setDateTime")) {
      this.props.navigation.getParam("setDateTime")(date)
    }
  }

  onPressDiary = (diaryId) => () => {
    this.props.navigation.navigate(VIEWS_NAME.DIARY_DETAIL, { diaryId })
  }

  onPressSearch = () => {
    this.props.navigation.navigate(VIEWS_NAME.SEARCH)
  }

  showDiaries = () => {
    if (this.state.diaries.length > 0) {
      const thisDiaries = {}
      this.state.diaries
        .map(diary => {
          if (thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")] instanceof Array) {
            thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")].push(diary)
          }
          else {
            thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")] = [];
            thisDiaries[moment(diary.diaryDate).format("dddd DD/MM")].push(diary)
          }
        })
      return thisDiaries
    }
    return {}
  }

  render() {
    const diaries = this.showDiaries()
    return (
      <Fragment>
        <Headers right={true} onRightPress={this.onPressSearch}>
          <MonthPicker onSetDate={this.onFilterMonthYear} date={this.state.date} />
        </Headers>
        {
          Object.keys(diaries).length === 0 ?
            <BlankDashboard /> :
            <ScrollView>
              {
                Object.keys(diaries).map((day, index) => {
                  if (diaries[day] instanceof Array) {
                    return (
                      <List.Section
                        key={index}
                        title={day}
                        titleStyle={{ paddingTop: 10, paddingBottom: 0, fontWeight: "bold", color: "#000" }}
                      >
                        {
                          diaries[day].map((diary) => {
                            return (
                              <CustomItem
                                key={diary.id}
                                time={moment(diary.diaryDate)}
                                timeline={true}
                                iconLeft={diary.mood}
                                midIcons={diary.activities.split(", ")}
                                content={diary.content}
                                onPress={this.onPressDiary(diary.id)}
                              />
                            )
                          })
                        }
                      </List.Section>
                    )
                  }
                  return null
                })
              }
              <View style={{ height: 30 }}></View>
            </ScrollView>
        }
      </Fragment>
    )
  }
} 