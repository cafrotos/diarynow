import React from 'react';
import { View, Image } from 'react-native'

import DashboardBg from 'assets/images/bg_dashboard.png'
import Title from 'components/Title';
import { ORIGINAL } from 'consts/colors';
import { getIcon } from 'utils/Utils';
import { ICON_TYPE } from 'consts/icons';

const BlankDashboard = (props) => {
  return (
    <View style={{ display: "flex", alignItems: "center", justifyContent: "flex-end" }}>
      <Image source={DashboardBg} style={{ height: "40%", width: "40%" }} />
      <Title size="large" color={ORIGINAL.BLACK_WHITE.LEVEL2} weight="bold">
        No Diaries
      </Title>
      <Title color={ORIGINAL.BLACK_WHITE.LEVEL2} >
        This month has no diaries
      </Title>
      <Title color={ORIGINAL.BLACK_WHITE.LEVEL2}>
        Add them now
      </Title>
      <View style={{paddingTop: 20}}>
        {
          getIcon({ type: ICON_TYPE.AntDesign, name: "arrowdown", size: 40, color: ORIGINAL.BLACK_WHITE.LEVEL2 })
        }
      </View>
    </View>
  )
}

export default BlankDashboard