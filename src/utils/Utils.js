import React, { Component } from 'react'
import { Image } from 'react-native'

import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Fontisto from 'react-native-vector-icons/Fontisto'
import Foundation from 'react-native-vector-icons/Foundation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Octicons from 'react-native-vector-icons/Octicons'
import ICONS, { ICON_TYPE } from 'consts/icons'
import { Avatar } from 'react-native-paper'
import { BG_MOOD_IMAGES } from 'consts';
import { ORIGINAL } from 'consts/colors'

export const getComponentFromProps = (props, defaultComponent) => {
  if (props === true) {
    return defaultComponent
  }
  return props

}

export const CustomComponent = (item = {}, CustomComp) => props => (
  <CustomComp {...props} {...item} />
)

/**
 * 
 * @param {String} key KEY_PARENT/KEY_CHILD
 * @param {Object} object 
 * @param {*} update
 */
export const updatePropsObjectByKey = (key, object, update) => {
  const keys = key.split("/");
  const lastKey = keys.pop();

  keys.unshift(object);
  const aaa = keys.reduce((c, n) => c[n]);
  aaa[lastKey] = update
  return object
}

/**
 * 
 * @param {Object} icon 
 * @param {String} icon.type
 * @param {String} icon.name
 * @param {Number} icon.size
 * @param {String} icon.color
 */
export const getIcon = (icon) => {
  switch (icon.type) {
    case ICON_TYPE.AntDesign: return <AntDesign name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.Entypo: return <Entypo name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.EvilIcons: return <EvilIcons name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.Feather: return <Feather name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.FontAwesome: return <FontAwesome name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.FontAwesome5: return <FontAwesome5 name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.Fontisto: return <Fontisto name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.Foundation: return <Foundation name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.Ionicons: return <Ionicons name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.MaterialCommunityIcons: return <MaterialCommunityIcons name={icon.name} size={icon.size || 15} color={icon.color} />
    case ICON_TYPE.MaterialIcons: return <MaterialIcons name={icon.name} size={icon.size || 15} color={icon.color} />
    default: return <Octicons name={icon.name} size={icon.size || 15} />
  }
}
/**
 * 
 * @param {String} title 
 * @param {Number} size 
 * @param {String} color 
 */
export const getIconByTitle = (title, size, color) => {
  if (typeof title !== "string" || title.trim() === "") {
    return null
  }
  color = color ? color : ICONS[title].color;
  return getIcon({ ...ICONS[title], size, color })
}

/**
 * 
 * @param {String} mood 
 */
export const getMoods = mood => {
  if (typeof mood !== 'string') {
    return null
  }
  switch (mood.toLowerCase()) {
    case "angry": return <Avatar.Image source={require('assets/images/emoji/angry.png')} />;
    case "confuse": return <Avatar.Image source={require('assets/images/emoji/confuse.png')} />;
    case "happy": return <Avatar.Image source={require('assets/images/emoji/happy.png')} />;
    case "sad": return <Avatar.Image source={require('assets/images/emoji/sad.png')} />;
    case "surprise": return <Avatar.Image source={require('assets/images/emoji/surprise.png')} />;
    case "worry": return <Avatar.Image source={require('assets/images/emoji/worry.png')} />;
    default: return null;
  }
}


const getBackgroundAngry = () => {
  switch (Math.floor(Math.random() * 1000 % 5) + 1) {
    case 1: return require("assets/images/background/angry/bg_angry_1.jpeg");
    case 2: return require("assets/images/background/angry/bg_angry_2.jpeg");
    case 3: return require("assets/images/background/angry/bg_angry_3.jpeg");
    case 4: return require("assets/images/background/angry/bg_angry_4.jpeg");
    case 5: return require("assets/images/background/angry/bg_angry_5.jpeg");
  }
}

const getBackgroundConfuse = () => {
  switch (Math.floor(Math.random() * 1000 % 6) + 1) {
    case 1: return require("assets/images/background/confuse/bg_confuse_1.jpg");
    case 2: return require("assets/images/background/confuse/bg_confuse_2.jpeg");
    case 3: return require("assets/images/background/confuse/bg_confuse_3.png");
    case 4: return require("assets/images/background/confuse/bg_confuse_4.jpg");
    case 5: return require("assets/images/background/confuse/bg_confuse_5.jpg");
    case 6: return require("assets/images/background/confuse/bg_confuse_6.jpeg");
    case 7: return require("assets/images/background/confuse/bg_confuse_7.png");
  }
}

const getBackgroundHappy = () => {
  switch (Math.floor(Math.random() * 1000 % 7) + 1) {
    case 1: return require("assets/images/background/happy/bg_happy_1.jpg");
    case 2: return require("assets/images/background/happy/bg_happy_2.jpg");
    case 3: return require("assets/images/background/happy/bg_happy_3.jpeg");
    case 4: return require("assets/images/background/happy/bg_happy_4.jpg");
    case 5: return require("assets/images/background/happy/bg_happy_5.jpg");
    case 6: return require("assets/images/background/happy/bg_happy_6.jpg");
    case 7: return require("assets/images/background/happy/bg_happy_7.jpg");
  }
}

const getBackgroundSurprise = () => {
  switch (Math.floor(Math.random() * 1000 % 5) + 1) {
    case 1: return require("assets/images/background/surprise/bg_surprise_1.jpeg");
    case 2: return require("assets/images/background/surprise/bg_surprise_2.jpg");
    case 3: return require("assets/images/background/surprise/bg_surprise_3.jpg");
    case 4: return require("assets/images/background/surprise/bg_surprise_4.jpg");
    case 5: return require("assets/images/background/surprise/bg_surprise_5.png");
  }
}

const getBackgroundWorry = () => {
  switch (Math.floor(Math.random() * 1000 % 6) + 1) {
    case 1: return require("assets/images/background/worry/bg_worry_1.jpg");
    case 2: return require("assets/images/background/worry/bg_worry_2.jpeg");
    case 3: return require("assets/images/background/worry/bg_worry_3.jpeg");
    case 4: return require("assets/images/background/worry/bg_worry_4.jpg");
    case 5: return require("assets/images/background/worry/bg_worry_5.jpg");
    case 6: return require("assets/images/background/worry/bg_worry_6.png");
  }
}

const getBackgroundSad = () => {
  switch (Math.floor(Math.random() * 1000 % 10) + 1) {
    case 1: return require("assets/images/background/sad/bg_sad_1.jpeg");
    case 2: return require("assets/images/background/sad/bg_sad_2.jpg");
    case 3: return require("assets/images/background/sad/bg_sad_3.jpg");
    case 4: return require("assets/images/background/sad/bg_sad_4.jpeg");
    case 5: return require("assets/images/background/sad/bg_sad_5.jpg");
    case 6: return require("assets/images/background/sad/bg_sad_6.jpg");
    case 7: return require("assets/images/background/sad/bg_sad_7.jpeg");
    case 8: return require("assets/images/background/sad/bg_sad_8.jpg");
    case 9: return require("assets/images/background/sad/bg_sad_9.png");
    case 10: return require("assets/images/background/sad/bg_sad_10.jpg");
  }
}

export const getBackgroundImage = mood => {
  if (typeof mood !== "string") {
    return null
  }
  switch (mood.toLowerCase()) {
    case "angry": return getBackgroundAngry();
    case "confuse": return getBackgroundConfuse();
    case "happy": return getBackgroundHappy();
    case "sad": return getBackgroundSad();
    case "surprise": return getBackgroundSurprise();
    case "worry": return getBackgroundWorry();
    default: return null;
  }
}